﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBettwentwoobjects : MonoBehaviour {

    public Transform player1;
    public Transform player2;

    private const float FOV_MARGIN = 15.0f;

    private Vector3 middlePoint;
    private float distanceFromMiddlePoint;
    private float distanceBetweenPlayers;
    private float aspectRatio;

    void Start()
    {
        aspectRatio = Screen.width / Screen.height;
    }

    void Update()
    {
        float followTimeDelta = 0.8f;
        float zoom = 1.5f;
        // Find the middle point between players.
        float x = (player2.position.x + player1.position.x) / 2;
        float y = (player2.position.y + player1.position.y) / 2;
        middlePoint = new Vector3(x, y);

        float distance = (player2.position - player1.position).magnitude;


        
        // Position the camera in the center.
        Vector3 newCameraPos = (middlePoint - Camera.main.transform.forward * distance * zoom);

        // Calculate the new FOV
        Camera.main.orthographicSize = distance;

        
        Camera.main.transform.position = (Vector3.Lerp((Camera.main.transform.position), newCameraPos, followTimeDelta));
    }
}
